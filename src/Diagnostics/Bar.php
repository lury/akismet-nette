<?php
/**
 * Created by PhpStorm.
 * User: LuRy <lury@lury.cz>
 * Date: 19.03.2017
 * Time: 11:19
 */

namespace LuRy\Akismet\Diagnostics;


use LuRy\Abra\AbraClient;
use LuRy\Akismet\Client;
use Tracy\Dumper;
use Tracy\IBarPanel;

class Bar implements IBarPanel {

    /** @var Client */
    private $akismetClient;
    /** @var array */
    private $config;

    public $panelWithResult = FALSE;

    public function __construct(Client $abraClient) {
        $this->akismetClient = $abraClient;
    }

    public function addConfig($config) {
        $this->config = $config;
    }
    /**
     * Renders HTML code for custom tab.
     * @return string
     */
    function getTab() {
        $diagnosticData = $this->akismetClient->getDiagnosticData();
        $img = base64_encode(file_get_contents(realpath(__DIR__)."/icon.png"));
        $data = sprintf('%0.1fms / %d', @$diagnosticData['timer']*1000, count($diagnosticData['history']));
        return '
            <span title="Akismet">
                <img src="data:image/png;base64,'.$img.'" title="Akismet" style="vertical-align: bottom;width: 1.23em;top:3px;left:-4px;float:left;" />
                '.$data.'
            </span>';

    }

    /**
     * Renders HTML code for custom panel.
     * @return string
     */
    function getPanel() {

        $history = '';
        $diagnosticData = $this->akismetClient->getDiagnosticData();
        foreach ($diagnosticData['history'] as $m ) {
            $timer = sprintf('%0.1fms', $m['timer']*1000);
            $history .= '
                    <tr>
                        <td>'.$timer.'</td>
                        <td>'.$m['method'].'</td>
                        <td>'.Dumper::toHtml($m['request']).'</td>
                        <td>'.Dumper::toHtml($m['response']).'</td>
                    </tr>
                ';
        }

        return '
            <h1 title="Akismet Connector">Akismet</h1>
            <table style="width:100%;">
                <tbody>
                    <tr>
                        <td>URL</td>
                        <td></td>
                        <td>'.$this->config['url'].'</td>
                    </tr>
                    <tr>
                        <td>Api key</td>
                        <td></td>
                        <td>'.$this->config['api-key'].'</td>
                    </tr>
                </tbody>
            </table>
            <table style="width:100%;">
                <thead>
                    <tr>
                        <th colspan="4">Calls</th>
                    </tr>
                </thead>
                <tbody>
                    '.$history.'
                </tbody>
            </table>
        ';
    }
}