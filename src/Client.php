<?php
/**
 * Created by PhpStorm.
 * User: LuRy <lury@lury.cz>
 * Date: 10.07.2018
 * Time: 23:34
 */

namespace LuRy\Akismet;


use AntonioTajuelo\Akismet\Akismet;
use LuRy\Akismet\Exception\AkismetClientException;
use Nette\SmartObject;
use Tracy\Debugger;

class Client {
    use SmartObject;

    /**
     * @var Akismet
     */
    private $akismet;

    private $loggingHam = FALSE;
    private $loggingSpam = FALSE;
    private $loggingCheck = FALSE;

    public function __construct($config) {
        $this->akismet = $akismet = new Akismet($config['api-key'],$config['url']);
        $this->loggingHam = ($config['logging'] ?? $config['logging']['ham'] ?? $config['logging']) == TRUE ? TRUE : FALSE;
        $this->loggingSpam = ($config['logging'] ?? $config['logging']['spam'] ?? $config['spam']) == TRUE ? TRUE : FALSE;
        $this->loggingCheck = ($config['logging'] ?? $config['logging']['check'] ?? $config['check']) == TRUE ? TRUE : FALSE;
    }

    /**
     * @return $this
     * @throws AkismetClientException
     */
    private function checkKey() {
        $response = $this->call('verifyKey');

        if ( $response !== 'valid' ) {
            throw new AkismetClientException('Api key is not valid');
        }
        return $this;
    }

    /**
     * @param $method
     * @param null $data
     * @return mixed
     */
    private function call($method, $data = NULL) {
        if ( $data === NULL ) {
            $response = call_user_func_array( [$this->akismet, $method ], []);
        } else {
            $response = call_user_func_array( [$this->akismet, $method ], [$data]);
        }
        return $response;
    }

    /**
     * @return array
     */
    public function getDiagnosticData() {
        return [
            'history' => $this->akismet->history,
            'timer' => $this->akismet->timer,
        ];
    }

    /**
     * @param Envelope $data
     * @return array
     * @throws \ReflectionException
     */
    private function buildEnvelope(Envelope $data) {
        return $data->toArray();
    }

    /**
     * @param Envelope $envelope
     * @return mixed
     * @throws AkismetClientException
     * @throws \ReflectionException
     */
    public function submitCheckComment(Envelope $envelope) {
        $this->checkKey();
        $data = $this->buildEnvelope($envelope);
        $response = $this->call('commentCheck', $data);
        if ( $this->loggingCheck ) {
            Debugger::log(["check" => $envelope], 'akismet');
        }
        return $response;
    }

    /**
     * @param Envelope $envelope
     * @return mixed
     * @throws AkismetClientException
     * @throws \ReflectionException
     */
    public function submitSpam(Envelope $envelope) {
        $this->checkKey();
        $data = $this->buildEnvelope($envelope);
        $response = $this->call('submitSpam', $data);
        if ( $this->loggingSpam ) {
            Debugger::log(["spam" => $envelope], 'akismet');
        }
        return $response;
    }

    /**
     * @param Envelope $envelope
     * @return mixed
     * @throws AkismetClientException
     * @throws \ReflectionException
     */
    public function submitHam(Envelope $envelope) {
        $this->checkKey();
        $data = $this->buildEnvelope($envelope);
        $response = $this->call('submitHam', $data);
        if ( $this->loggingHam ) {
            Debugger::log(["ham" => $envelope], 'akismet');
        }
        return $response;
    }


}