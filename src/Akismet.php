<?php
/**
 * User: Antonio Tajuelo <atajuelo@gmail.com>
 */

namespace AntonioTajuelo\Akismet;

use Tracy\Debugger;

class Akismet
{

    private $key  = '';
    private $blog = '';

    public $calls = 0;
    public $timer = 0;

    public $history = [];

    function __construct($key = '',$blog = '')
    {
        $this->key  = $key;
        $this->blog = $blog;
    }

    private function request($endpoint,$params = [])
    {
        $client = new \GuzzleHttp\Client(['http_errors' => false]);
        $form_params = array_merge(['blog' => $this->blog],$params);
        Debugger::timer(__CLASS__);

        $request = $client->request(
            'POST',
            $endpoint,
            [
                'form_params' => $form_params
            ]
        );
        $response = (string)$request->getBody();

        $timer = Debugger::timer(__CLASS__);
        $this->history[] = [
            'method' => $endpoint,
            'request' => $form_params,
            'timer' => $timer,
            'response' => $response
        ];
        $this->timer += $timer;
        
        return $response;
    }

    public function verifyKey()
    {
        return $this->request(
            'https://rest.akismet.com/1.1/verify-key',
            ['key' => $this->key]
        );
    }

    public function commentCheck($params)
    {
        return $this->request(
            'https://' . $this->key . '.rest.akismet.com/1.1/comment-check',
            $params
        );
    }

    public function submitSpam($params)
    {
        return $this->request(
            'https://' . $this->key . '.rest.akismet.com/1.1/submit-spam',
            $params
        );
    }

    public function submitHam($params)
    {
        return $this->request(
            'https://' . $this->key . '.rest.akismet.com/1.1/submit-ham',
            $params
        );
    }

}