<?php
/**
 * Created by PhpStorm.
 * User: LuRy <lury@lury.cz>
 * Date: 17.04.2017
 * Time: 18:56
 */

namespace LuRy\Akismet\DI;


use LuRy\Akismet\Diagnostics\Bar;
use LuRy\Akismet\Exception\AkismetConfigException;
use Nette\DI\CompilerExtension;
use Nette\PhpGenerator\ClassType;

class AkismetExtension extends CompilerExtension {

    public function loadConfiguration() {
        $builder = $this->getContainerBuilder();
        $config = $this->getConfig();

        if ( !isset($config['api-key']) ) {
            throw new AkismetConfigException('Akismet: Missing api-key');
        }

        if ( !isset($config['url']) ) {
            throw new AkismetConfigException('Akismet: Missing blog url');
        }

        $builder->addDefinition($this->prefix('client'))
            ->setClass(\LuRy\Akismet\Client::class, [ $config ])
            ->setAutowired(TRUE);

        $builder->addDefinition($this->prefix('akismetBar'))
            ->setClass(Bar::class)
            ->addSetup('?->addConfig(?)', ['@self', $config]);
            //->addSetup('?->panelWithResult = ?', [ '@self' ]);
    }

    public function afterCompile(ClassType $classType) {

        $initialize = $classType->methods['initialize'];
        $initialize
            ->addBody('Tracy\Debugger::getBar()->addPanel($this->getService(?));', [ $this->prefix('akismetBar') ]);

    }

}