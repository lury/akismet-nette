<?php
/**
 * Created by PhpStorm.
 * User: LuRy <lury@lury.cz>
 * Date: 11.07.2018
 * Time: 1:06
 */

namespace LuRy\Akismet;


use LuRy\Akismet\Exception\AkismetClientException;
use Nette\SmartObject;
use Nette\Utils\ArrayHash;
use Nette\Reflection\ClassType;


/**
 * Class Envelope
 * @package LuRy\Akismet
 */
class Envelope extends ArrayHash {

    use SmartObject;

    /**
     * @required
     * @var string
     */
    //protected $blog;
    /**
     * @required
     * @var string
     */
    protected $user_ip;
    /**
     * @required
     * @var string
     */
    protected $user_agent;
    /**
     * @var string
     */
    protected $comment_type;
    /**
     * @var string
     */
    protected $comment_content;

    /**
     * @var string
     */
    protected $permalink;
    /**
     * @var string
     */
    protected $comment_author;
    /**
     * @var string
     */
    protected $comment_author_url;
    /**
     * @var string
     */
    protected $comment_date_gmt;
    /**
     * @var string
     */
    protected $comment_post_modified_gmt;
    /**
     * @var string
     */
    protected $blog_lang;
    /**
     * @var string
     */
    protected $blog_charset = 'UTF-8';
    /**
     * @var string
     */
    protected $user_role;
    /**
     * @var string
     */
    protected $is_test;

    /**
     * @return array
     * @throws AkismetClientException
     */
    public function toArray() {
        $data = [];

        foreach ( $this->getReflection()->getProperties(\ReflectionProperty::IS_PROTECTED) as $property ) {
            $value = $this->{$property->name};
            if ( $value !== NULL ) {
                $data[$property->name] = $value;
            } else if ( $this->getReflection()->getProperty($property->name)->hasAnnotation('required') ) {
                throw new AkismetClientException("Missing required parameter '{$property->name}' in request envelope.");
            }
        }
        return $data;
    }

    /**
     * @param string $user_ip
     * @return Envelope
     */
    public function setUserIp($user_ip)
    {
        $this->user_ip = $user_ip;
        return $this;
    }

    /**
     * @param string $user_agent
     * @return Envelope
     */
    public function setUserAgent($user_agent)
    {
        $this->user_agent = $user_agent;
        return $this;
    }

    /**
     * @param string $comment_type
     * @return Envelope
     */
    public function setCommentType($comment_type)
    {
        $this->comment_type = $comment_type;
        return $this;
    }

    /**
     * @param string $comment_content
     * @return Envelope
     */
    public function setCommentContent($comment_content)
    {
        $this->comment_content = $comment_content;
        return $this;
    }

    /**
     * @param string $permalink
     * @return Envelope
     */
    public function setPermalink($permalink)
    {
        $this->permalink = $permalink;
        return $this;
    }

    /**
     * @param string $comment_author
     * @return Envelope
     */
    public function setCommentAuthor($comment_author)
    {
        $this->comment_author = $comment_author;
        return $this;
    }

    /**
     * @param string $comment_author_url
     * @return Envelope
     */
    public function setCommentAuthorUrl($comment_author_url)
    {
        $this->comment_author_url = $comment_author_url;
        return $this;
    }

    /**
     * @param string $comment_date_gmt
     * @return Envelope
     */
    public function setCommentDateGmt($comment_date_gmt)
    {
        $this->comment_date_gmt = $comment_date_gmt;
        return $this;
    }

    /**
     * @param string $comment_post_modified_gmt
     * @return Envelope
     */
    public function setCommentPostModifiedGmt($comment_post_modified_gmt)
    {
        $this->comment_post_modified_gmt = $comment_post_modified_gmt;
        return $this;
    }

    /**
     * @param string $blog_lang
     * @return Envelope
     */
    public function setBlogLang($blog_lang)
    {
        $this->blog_lang = $blog_lang;
        return $this;
    }

    /**
     * @param string $blog_charset
     * @return Envelope
     */
    public function setBlogCharset($blog_charset)
    {
        $this->blog_charset = $blog_charset;
        return $this;
    }

    /**
     * @param string $user_role
     * @return Envelope
     */
    public function setUserRole($user_role)
    {
        $this->user_role = $user_role;
        return $this;
    }

    /**
     * @param string $is_test
     * @return Envelope
     */
    public function setIsTest($is_test)
    {
        $this->is_test = $is_test;
        return $this;
    }

    public static function getReflection(){
	    return ClassType::from(get_called_class());
    }


}